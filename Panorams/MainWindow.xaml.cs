﻿using dCountPanels;
using OPForGrozaC;
using SpectrumPanoramaControl;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Panorams
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DFTcpClient client;

        DynamicPanels<SPControl> dynamicPanels;
        Random random = new Random();

        int countControl;
        int port = 32123;
        string ipServer = "127.0.0.1";


        double[] specLoc;

        public MainWindow()
        {
            InitializeComponent();
            Calc calc = new Calc() { Orientation = dCountPanels.Orientation.Vertical, Dimension = Dimension.CustomColumn };
            dynamicPanels = new DynamicPanels<SPControl>(DynamicGrid, calc);
            countControl = 1;
            dynamicPanels.DoIt(countControl);
            hidePanelsInSPControl();



            pLibrary.GlobalNumberOfBands = 10;
            pLibrary.GlobalBandWidthMHz = 100;
            pLibrary.GlobalRangeXmin = 0;

            pLibrary.GlobalRangeYmin = 0;
            pLibrary.GlobalRangeYmax = 1_000_0;

            

            pLibrary.BandOnRS += PLibrary_BandOnRS;
            pLibrary.OnFreqArea += PLibrary_OnFreqArea;
            btnConnector.LedWriteDataHint = "Write";
            btnConnector.LedReadDataHint = "Read";
            btnConnector.ButServerHint = "Server";
            btnConnector.LabelHint = "Action";

            btnConnector.ShowDisconnect();

        }

        private void PLibrary_OnFreqArea(object sender, PanoramaLibrary.PLibrary.FrequencyType frequencyType, double startFreq, double endFreq)
        {
            countControl++;
            dynamicPanels.DoIt(countControl);
            hidePanelsInSPControl();
        }

        private void PLibrary_BandOnRS(object sender, double freqLeftMHz, double freqRightMHz)
        {
            throw new NotImplementedException();
        }

        private void btnRandom_Click(object sender, RoutedEventArgs e)
        {
            countControl = random.Next(1, 10);
            dynamicPanels.DoIt(countControl);
            hidePanelsInSPControl();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (countControl == 1)
                return;
            countControl--;
            dynamicPanels.DoIt(countControl);
            hidePanelsInSPControl();

        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            countControl++;
            dynamicPanels.DoIt(countControl);

            hidePanelsInSPControl();
        }

        void hidePanelsInSPControl()
        {
            for (int i = 0; i < dynamicPanels.Count(); i++)
            {
                dynamicPanels[i].MinHeight = 200;
                dynamicPanels[i].topPanelVisible = false;
                dynamicPanels[i].firstPanelVisible = false;
            }
        }

        private void btnStopScroll_Click(object sender, RoutedEventArgs e)
        {
            sv.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
        }

        private void btnStartScroll_Click(object sender, RoutedEventArgs e)
        {
            sv.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
        }

        private void btnConnector_Click(object sender, RoutedEventArgs e)
        {

            if (btnConnector.ServerConnectionState == WPFControlConnection.ConnectionStates.Connected)
                controlDisconnect();
            else
                controlConnect();
        }

        void controlDisconnect()
        {
            btnConnector.LabelText = "Connect";
            btnConnector.ShowDisconnect();
            client?.Stop();
        }

        void controlConnect()
        {
            try
            {
                btnConnector.LabelText = "Disconnect";
                btnConnector.ShowConnect();
                client = new DFTcpClient(ipServer, port, 1, 2);
                
                client.Disconnect += Client_Disconnect;
                client.GetResponse += Client_GetResponse;
                client.GetSpectrum += Client_GetSpectrum;
                client.Read += Client_Read;
                client.Write += Client_Write;

                client.isAutoSend = true;
                client.isFloat = true;

                client.Start();
            }
            catch (Exception error)
            {
                Client_Disconnect(error.Message);
            }

        }

        private void Client_Write()
        {
            btnConnector.Dispatcher.Invoke(() =>
                btnConnector.ShowWrite()
            );
        }

        private void Client_Read()
        {
            btnConnector.Dispatcher.Invoke(() =>
                btnConnector.ShowRead()
            );
        }

        private void Client_GetSpectrum(dynamic[] firstSpectrum, dynamic secondSpectrum)
        {
            double[] floatSpectrumFirst = new double[firstSpectrum.Length];
            double[] floatSpectrumSecond = new double[firstSpectrum.Length];

            for(int i = 0; i < firstSpectrum.Length; i++)
            {
                floatSpectrumFirst[i] = firstSpectrum[i];
                floatSpectrumSecond[i] = secondSpectrum[i];
            }

            pLibrary.Dispatcher.Invoke(() =>
                pLibrary.spControl.PlotSpectr(0, 100, floatSpectrumFirst.Concat(floatSpectrumSecond).ToArray())
            );
        }

        private void Client_GetResponse(byte[] response)
        {
            throw new NotImplementedException();
        }

        private void Client_Disconnect(string error)
        {
            controlDisconnect();
        }

        private void btnGetSpectr_Click(object sender, RoutedEventArgs e)
        {
            client?.RequestSpectrumAuto();
        }
    }
}
